
DROP PROCEDURE IF EXISTS ActualizaLimCred;

DELIMITER |

CREATE PROCEDURE ActualizaLimCred()
BEGIN

    DECLARE numCliente INT;
    DECLARE x INT;
    DECLARE Porcentaje NUMERIC(15,2);
    DECLARE Fecha DATE;
    DECLARE ID INT; /*ID del Cliente */
    DECLARE AumenLim NUMERIC(15,2);

    SET numCliente = (SELECT COUNT(*) FROM Clientes ORDER BY CodigoCliente); 
    SET Fecha = (SELECT CURDATE()); 

    SET x = 1;

    while x <= numCliente do

    SET ID = (SELECT CodigoCliente FROM Clientes WHERE CodigoCliente = x ORDER BY CodigoCliente); /*Codigo de todos los Clientes*/

    IF ID IS NOT NULL then
    
    SET Porcentaje = (SELECT SUM(Cantidad) *0.15 AS 'Cantidad del 15%' FROM Pagos WHERE CodigoCliente = x AND YEAR(FechaPago)
    BETWEEN 2008 AND 2010 GROUP BY CodigoCliente); /*15% de la cantidad gastada entre 2008 y 2010*/
    
    end IF;
/*----------------------------------------------------------*/

    IF Porcentaje IS NULL then /* Si el cliente no ha realizado pedidos en esos años no se le aumenta el limite de credito y se le queda a 0*/
      
    SET Porcentaje = 0;
    
    end IF;
/*----------------------------------------------------------*/

    IF ID IS NOT NULL then /* Guardar datos en la tabla ActualizacionLimiteCredito */ 
     
    INSERT INTO ActualizacionLimiteCredito VALUES (Fecha, ID, Porcentaje);
   
    end IF;
/*----------------------------------------------------------*/

    IF ID IS NOT NULL then /*Aumento de credito de los clientes*/
     
    SET AumenLim = (SELECT Incremento FROM ActualizacionLimiteCredito WHERE CodigoCliente = x);
   
    end IF;
/*----------------------------------------------------------*/

    IF ID IS NOT NULL then /*suma del incremento + limite de credito*/

    UPDATE Clientes SET LimiteCredito = LimiteCredito + Porcentaje WHERE CodigoCliente = x;

    end IF;
/*---------------------------------------------------------*/
    SET x = x + 1;

    end while;

END;

|

DELIMITER ;

CALL ActualizaLimCred();










